# Dungeoneer Rule Fixes and Mods

The [Dungeoneer](http://www.atlas-games.com/dungeoneer) card game by
Atlas Games is fun, but the rules can be complex. You shouldn't let
that scare you away from a great game, though.

So that it doesn't, I've written this translation for people who want
to play **Dungeoneer** but find the rules daunting.

## What Do I Download?

You should download PDFs (for computer screens or for print) and EPUBs
(for mobiles and ereaders) from the `dist` directory.

Everything else in this repository is source code for the documents;
only download that if you are interested in modifying what I've done.

If you have questions, feel free to ask.

Everything below this line is for nerds, or the nerdcurious:

## What's Included

This repository includes:

* a "translation" (a re-write from *pro gamer* to normal human) of
  the standard **Dungeoneer** rules. This is not a mod, it's the rules,
  written to be user-friendly.

* a single-player mod adapted from some fellow users of
  [boardgamegeek.com](http://boardgamegeek.com). It's in a separate file
  called `solo.xml` but since I have re-organised and clarified it, my
  version does contain some parts of my general re-write.

* brand new rules for solo (can easily be adapted for
  two-players). This happens to currently use the **Dungeoneer** card
  set, but in the future I may develop cards or software for it such
  that it can be its own game.

You can't miss it, and the sources are clearly marked.


## Source Code (Docbook)

The rules are written (or transcribed, you might argue in the case of
the solo rules) in [docbook](http://docbook.org) because markdown is
broken. There, I said it.

If you want to argue with me on that, roll a d6 and add the complexity
of your document to your total. Send me the results; if you win, we'll
talk. If I win, it's time for you to learn XML.

Otherwise, build with [xmlto](https://fedorahosted.org/xmlto) or
[fop](https://xmlgraphics.apache.org/fop). Requires
[Docbook](http://www.docbook.org/tdg5/en/html/appa.html).
